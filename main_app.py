#!/usr/bin/env python
# -*- coding: utf-8 -*-
import tkinter
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from annotation import annotation
from functools import partial

ann_instance = annotation()
def app():
    messagebox.showinfo(shot_status_comb.get())

# メインウィンドウ
main_win = tkinter.Tk()
main_win.title("アノテーションを実行")
main_win.geometry("500x500")

# メインフレーム
main_frm = ttk.Frame(main_win)
main_frm.grid(column=0, row=0, sticky=tkinter.NSEW, padx=5, pady=10)

# ウィジェット作成（開始と再開）
start_btn1 = ttk.Button(main_frm, text="最初から始める", command=ann_instance.start)
start_btn2 = ttk.Button(main_frm, text="続きから始める", command=ann_instance.restart)

# ウィジェット作成（ショットの分類）
shot_status_label = ttk.Label(main_frm, text="ショットの状態分類")
shot_status_comb = ttk.Combobox(main_frm, values=[1, 2, 3, 4, 5, 6, 7], width=10)
shot_status_comb.current(0)

# ウィジェット作成（ショットの種類）
shot_type_label = ttk.Label(main_frm, text="ショットの種類")
shot_type_comb = ttk.Combobox(main_frm, values=[-1, 1, 2, 3, 4, 5, 6, 7], width=10)
shot_type_comb.current(0)

# ウィジェット作成（ショットの種類）
dominant_hand_label = ttk.Label(main_frm, text="利き手")
dominant_hand_comb = ttk.Combobox(main_frm, values=["r","l"], width=10)
dominant_hand_comb.current(0)

# ウィジェット作成（次へ）
next_btn = ttk.Button(main_frm, text="次へ", command=partial(partial(partial(ann_instance.next, shot_status=shot_status_comb), shot_type=shot_type_comb), dominant_hand=dominant_hand_comb))

# ウィジェット作成（次の動画ボタン）
next_movie_btn = ttk.Button(main_frm, text="次の動画へ", command=ann_instance.next_movie)

# やめるボタン
stop_btn = ttk.Button(main_frm, text="作業終了", command=ann_instance.stop)

# やり直しボタン
redo_btn = ttk.Button(main_frm, text="やり直し", command=ann_instance.redo)

# 一つ前に戻るボタン
undo_btn = ttk.Button(main_frm, text="前へ", command=ann_instance.undo)

# nフレーム早送りボタン
next_nframe_label = ttk.Label(main_frm, text="フレーム数")
next_nframe_comb = ttk.Combobox(main_frm, values=[5, 10, 20, 30, 40, 50], width=10)
next_nframe_comb.current(0)
next_nframe_btn = ttk.Button(main_frm, text="複数フレーム進める", command=partial(partial(partial(partial(ann_instance.next_nframe, shot_status=shot_status_comb), shot_type=shot_type_comb), dominant_hand=dominant_hand_comb), skip_frame=next_nframe_comb))

# 複数前に戻るボタン
undo_nframe_btn = ttk.Button(main_frm, text="複数フレーム戻す", command=partial(ann_instance.undo_nframe, undo_frame=next_nframe_comb))

# ラベル
label1 = ttk.Label(main_frm, text = "      ")
label2 = ttk.Label(main_frm, text = "      ")
label3 = ttk.Label(main_frm, text = "      ")
label4 = ttk.Label(main_frm, text = "      ")

# ウィジェットの配置
start_btn1.grid(column=0, row=0, pady=10)
start_btn2.grid(column=1, row=0, pady=10)
shot_status_label.grid(column=0, row=1, pady=10)
shot_status_comb.grid(column=1, row=1, sticky=tkinter.W, padx=5)
shot_type_label.grid(column=0, row=2)
shot_type_comb.grid(column=1, row=2, sticky=tkinter.W, padx=5)
dominant_hand_label.grid(column=0, row=3)
dominant_hand_comb.grid(column=1, row=3, sticky=tkinter.W, padx=5)
undo_btn.grid(column=0, row=4)
next_btn.grid(column=1, row=4)
redo_btn.grid(column=2, row=4)


next_nframe_label.grid(column=0, row=7, pady=10)
next_nframe_comb.grid(column=1, row=7, sticky=tkinter.W, padx=5)
undo_nframe_btn.grid(column=0, row=8)
next_nframe_btn.grid(column=1, row=8)
next_movie_btn.grid(column=1, row=10)
stop_btn.grid(column=2, row=10)
label1.grid(column=0, row=9)






# 配置設定
main_win.columnconfigure(0, weight=1)
main_win.rowconfigure(0, weight=1)
main_frm.columnconfigure(1, weight=1)

main_win.mainloop()


def key_event(e):
   key = e.keysym
   if key == "Up":
       y -= 10
   if key == "Down":
       y += 10
   if key == "Left":
       x -= 10
   if key == "Right":
       x += 10