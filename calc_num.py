#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import cv2
import click
import time
import shutil
import pathlib
import pandas as pd

def save_all_frames(video_path, dir_path, basename, ext="jpg"):
    cap = cv2.VideoCapture(video_path)
    count = cap.get(cv2.CAP_PROP_FRAME_COUNT)
    return count
    


def main():
    list_df = pd.read_csv("data/list.csv", header = None)
    list_df.columns = ["path", "is_finish"]
    annotation_results, cnt = [], 0
    frame_nums = 0
    for i in list_df.query("is_finish == False").index:
        f = list_df.iloc[i, 0]
        frame_nums += save_all_frames(f, "./image", f.strip("movie/").split(".")[0])
    print(frame_nums)

if __name__ == "__main__":
    main()
