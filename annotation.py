#!/usr/bin/env python
# -*- coding: utf-8 -*-
import glob
import pandas as pd
import cv2
import os
import pathlib
import shutil
from tkinter import messagebox
import sys
import boto3
import time
import threading

class annotation:

    def __init__(self) -> None:
        self.key_dict = {49: 1, 50: 2, 51: 3, 52: 4, 53: 5, 54: 6, 55: 7, 56: 8, 57: 9, 58: 0}
        self.movie_index = 0
        self.res = []
        self.res2 = []
        self.res3 = []
        self.image_index = 0
        self.target_file_path = sorted(glob.glob("./target/*"))[0]

    def start(self): # 複数の動画に対して一度だけ実行する
        #self._make_list()
        self._read_list()
        self._main()

    def _make_list(self): # フォルダ中の動画リストを作成する
        annotation_list = list()
        movie_paths = sorted(glob.glob("./movie/*"))
        for f in movie_paths:
            annotation_list.append([str(f).replace("\\","/"), False])
        annotation_df = pd.DataFrame(annotation_list, columns=["path", "is_finish"])
        annotation_df.to_csv("data/list.csv", mode="w", header=False, index=False)

    def _read_list(self):
        target: pd.DataFrame = pd.read_csv(self.target_file_path)
        target = target[['動画ID', '動画Path']]
        target = target.drop_duplicates()
        target["is_finish"] = False
        target.to_csv("data/list.csv", mode="w", header=False, index=False)


    def restart(self):# 作業を再開した際に最初に実行する
        self._main()

    def _save_all_frames(self, video_path, dir_path, basename, ext="jpg"): # 動画をフレーム画像にして保存する
        cap = cv2.VideoCapture(video_path)

        if not cap.isOpened():
            return

        os.makedirs(dir_path, exist_ok=True)
        base_path = os.path.join(dir_path, basename)

        digit = len(str(int(cap.get(cv2.CAP_PROP_FRAME_COUNT))))

        n = 0

        while True:
            ret, frame = cap.read()
            if ret:
                cv2.imwrite("{}_{}.{}".format(base_path, str(n).zfill(digit), ext), frame)
                n += 1
            else:
                print("画像化終了")
                return
        

    def _annotation(self): # 画像をウィンドウで開きアノテーション作業をさせる
        try:
            image_file = sorted(pathlib.Path("image").iterdir())[self.image_index]
            img = cv2.imread(str(image_file), cv2.IMREAD_COLOR)
            cv2.namedWindow(str(image_file), cv2.WINDOW_NORMAL)
            cv2.resizeWindow(str(image_file), 320 * 3, 240 * 3)
            cv2.imshow(str(image_file), img)
        except:
            cv2.destroyAllWindows()
            self._fin_process()
    
    def next(self, shot_status, shot_type, dominant_hand): # 次の画像を開く
        cv2.destroyAllWindows()
        self.image_index += 1
        self.res.append(shot_status.get())
        self.res2.append(shot_type.get())
        self.res3.append(dominant_hand.get())
        self._annotation()

    def next_nframe(self, shot_status, shot_type, dominant_hand, skip_frame):
        cv2.destroyAllWindows()
        try:
            for f in range(0, int(skip_frame.get())):
                self.image_index += 1
                self.res.append(shot_status.get())
                self.res2.append(shot_type.get())
                self.res3.append(dominant_hand.get())
                # 最後のフレームの時はこのフレームで落ちる
                image_file = sorted(pathlib.Path("image").iterdir())[self.image_index]
            self._annotation()
        except:
            cv2.destroyAllWindows()
            self._fin_process()
        

    def _main(self): # それぞれの動画に対して最初一度行う処理
        self.list_df = pd.read_csv("data/list.csv", header = None)
        self.list_df.columns = ["動画ID", "動画Path", "is_finish"]
        self.annotation_results = []
        self.i = self.list_df.query("is_finish == False").index[0]
        self.movie_id = self.list_df.iloc[self.i, 0]
        parts = str(self.list_df.iloc[self.i, 1]).replace("s3://", "").split("/", 1)
        self.bucket_name = parts[0]
        self.s3_key = parts[1]
        self._download_mp4(self.s3_key, self.movie_id)
        thread1 = threading.Thread(target=self._save_all_frames, args=(self.save_video_path, "./image/", str(self.movie_id)))
        thread1.start()
        time.sleep(1)
        self._enter_face_point()
        self._annotation()
    
    def _fin_process(self):# 終わり際に実行する処理
        shutil.rmtree("./image")
        self.annotation_results.extend(self.res)
        # 動画リスト上で処理完了にする
        self.list_df.iloc[self.i, 2] = True
        self._output(self.list_df, [[self.s3_key, i, self.res[i], self.res2[i], self.res3[i], self.points[-1][0], self.points[-1][1]] for i in range(len(self.res))])
        self.image_index = 0
        self.res = []
        self.res2 = []
        self.res3 = []
        ret = messagebox.askyesno("完了", "アノテーションの保存とアップロードが完了しました\n次の動画をアノテーションしますか?")
        if ret == True:
            self.next_movie()

    def _output(self, list_df, annotation_results): # アノテーション結果の保存
        list_df.to_csv("data/list.csv", mode="w", header=False, index=False)
        df = pd.DataFrame(annotation_results)
        os.makedirs('./output', exist_ok=True)
        df.to_csv("./output/"+ str(self.movie_id) + ".csv", mode="w", header=False, index=False)
        self._upload_result(movie_id= str(self.movie_id))
    
    def next_movie(self): # 次の動画を開く
        self._main()
        
    def stop(self):
        cv2.destroyAllWindows()
        try:
            shutil.rmtree("./image")
        except:
            pass
        sys.exit()
        
    def redo(self):
        cv2.destroyAllWindows()
        self.res = []
        self.res2 = []
        self.res3 = []
        self.image_index = 0
        self._main()
    
    def undo(self):
        if self.image_index == 0:
            messagebox.showinfo("不明な操作","一枚目の画像です")
        else:
            cv2.destroyAllWindows()
            self.image_index -= 1
            self.res.pop(-1)
            self.res2.pop(-1)
            self.res3.pop(-1)
            self._annotation()
    
    def undo_nframe(self, undo_frame):
        if self.image_index - int(undo_frame.get()) < 0:
            messagebox.showinfo("不明な操作","戻りすぎています\n適切なフレーム数を選んで下さい")
        else:
            cv2.destroyAllWindows()
            for i in range(0, int(undo_frame.get())):
                self.image_index -= 1
                self.res.pop(-1)
                self.res2.pop(-1)
                self.res3.pop(-1)
            self._annotation()

    def _download_mp4(
        self,
        s3_key: str = None,
        movie_id: str = None
    ) -> None:
        self.save_video_path = f'movie/{movie_id}.mp4'
        os.makedirs('./movie', exist_ok=True)
        if os.path.isfile(self.save_video_path) == False:
            print("ダウンロードを開始します")
            s3 = boto3.resource("s3")
            bucket = s3.Bucket(self.bucket_name)
            bucket.download_file(s3_key, self.save_video_path)
        else:
            print("ダウンロード済み")
    
    def _upload_result(
        self,
        movie_id: str = None,
    ):
        s3 = boto3.resource("s3")
        bucket = s3.Bucket(self.bucket_name)
        bucket.upload_file(
            f"output/{movie_id}.csv",
            f"skeleton/USER_STG/{movie_id}/annotation_results.csv"
        )

    def _enter_face_point(self):
        messagebox.showinfo("アノテーション人物の決定","人物の顔付近をクリックしてください\n最後のクリックが反映されます\n終了する際はzキーを押してください")
        def mouse_event(event,x,y, flags, param):
            if event == cv2.EVENT_LBUTTONUP:
                points.append([x,y])
                print(x,y)
                cv2.circle(img, (x,y), 5, (255,0,0), -1)
        points = []
        image_file = sorted(pathlib.Path("image").iterdir())[self.image_index]
        img = cv2.imread(str(image_file), cv2.IMREAD_COLOR)
        cv2.namedWindow(str(image_file), cv2.WINDOW_NORMAL)
        cv2.resizeWindow(str(image_file), 320 * 3, 240 * 3)
        cv2.imshow(str(image_file), img)
        cv2.setMouseCallback(str(image_file), mouse_event)
        while True:
            cv2.imshow(str(image_file), img) 
            if cv2.waitKey(1) & 0xFF == ord("z"):
                break
        self.points = points
        messagebox.showinfo("アノテーション人物の決定","ショットの状態分類、ショットの種類、利き手を設定した後\n「次へ」ボタンを押してアノテーションを開始してください")


