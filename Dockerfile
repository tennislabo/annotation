#
FROM python:3.6
WORKDIR /app

RUN apt-get update && \
    apt-get install -y libopencv-dev && \
    set -x 

COPY requirements.txt /app/
COPY run.py /app/
ADD movie/ /app/movie/
ADD data/ /app/data/

RUN pip install --upgrade pip setuptools \
    pip install -r requirements.txt 

CMD ["python", "run.py", "--start"]