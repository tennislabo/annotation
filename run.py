#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys
import cv2
import click
import time
import shutil
import pathlib
import glob
import pandas as pd

def save_all_frames(video_path, dir_path, basename, ext="jpg"):
    cap = cv2.VideoCapture(video_path)

    if not cap.isOpened():
        return

    os.makedirs(dir_path, exist_ok=True)
    print("a")
    base_path = os.path.join(dir_path, basename)

    digit = len(str(int(cap.get(cv2.CAP_PROP_FRAME_COUNT))))

    n = 0

    while True:
        ret, frame = cap.read()
        if ret:
            cv2.imwrite("{}_{}.{}".format(base_path, str(n).zfill(digit), ext), frame)
            n += 1
        else:
            return


def make_list():
    movie_dir_path = pathlib.Path("movie")
    annotation_list = list()
    aaa = sorted(glob.glob("./movie/*"))
    print(aaa)
    for f in aaa:
        print(str(f))
        annotation_list.append([str(f), False])
    annotation_df = pd.DataFrame(annotation_list, columns=["path", "is_finish"])
    annotation_df.to_csv("data/list.csv", mode="w", header=False, index=False)


key_dict = {49: 1, 50: 2, 51: 3, 52: 4, 53: 5, 54: 6, 55: 7, 56: 8, 57: 9, 58: 0}


def annotation():
    n = 0
    res = []
    for image_file in sorted(pathlib.Path("image").iterdir()):
        img = cv2.imread(str(image_file), cv2.IMREAD_COLOR)
        cv2.namedWindow(str(image_file), cv2.WINDOW_NORMAL)
        cv2.resizeWindow(str(image_file), 320 * 3, 240 * 3)
        cv2.imshow(str(image_file), img)
        input_key = cv2.waitKey(0)
        if input_key == 27:
            return False, res
        res.append(key_dict.get(input_key, -1))
        cv2.destroyAllWindows()
        n += 1
    return True, res


def output(list_df, annotation_results):
    list_df.to_csv("data/list.csv", mode="w", header=False, index=False)
    df = pd.DataFrame(annotation_results)
    df.to_csv("data/annotation_result.csv", mode="a", header=False, index=False)
    annotation_results = []


@click.command()
@click.option("--start/--restart", default=False)
def main(start):
    if start:
        make_list()
    list_df = pd.read_csv("data/list.csv", header = None)
    list_df.columns = ["path", "is_finish"]
    annotation_results, cnt = [], 0
    for i in list_df.query("is_finish == False").index:
        
        f = list_df.iloc[i, 0]
        save_all_frames(f, "./image", f.strip("movie/").split(".")[0])
        r, res_list = annotation()
        shutil.rmtree("./image")
        if not r:
            output(list_df, [[f, i, res_list[i]] for i in range(len(res_list))])
            sys.exit(1)
        annotation_results.extend(res_list)
        list_df.iloc[i, 1] = True
        output(list_df, [[f, i, res_list[i]] for i in range(len(res_list))])


def viz():
    # save_all_frames("movie/digonal_kyouda_0320_ipad_0.mp4", "./image", "digonal_kyouda_0320_ipad_0", ext="jpg")
    img = cv2.imread("image/digonal_kyouda_0320_ipad_0_00.jpg", cv2.IMREAD_COLOR)
    cv2.namedWindow("img_title", cv2.WINDOW_NORMAL)
    cv2.resizeWindow("img_title", 320 * 3, 240 * 3)
    cv2.imshow("img_title", img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    a = input()
    print(a)


if __name__ == "__main__":
    main()
    # viz()
